import json
import sys
import re
import html


def html_generation(data):
    if isinstance(data, list):
        htmloutput = '<ul>'
        for sourceObject in data:
            htmloutput += '<li>'
            for keyobject in sourceObject:
                if isinstance(sourceObject[keyobject], list):
                    htmloutput += html_generation(sourceObject[keyobject])
                else:
                    htmloutput += tag_init(keyobject, sourceObject[keyobject])
            htmloutput += '</li>'
        htmloutput += '</ul>'
    else:
        for keyObject in data:
            htmloutput = tag_init(keyObject, data[keyObject])
    return htmloutput


def tag_init(tag, content):
    id = re.findall('\#[a-zA-Z]*', tag)
    classes = re.findall('\.[a-zA-Z]*', tag)
    printtag = re.split('\.|#', tag)
    printtag = printtag[0]
    printid = ""
    printclass = ""
    if len(id) > 0:
        printid = ' id="'+id[0]+'" '
        printid = printid.replace('#', '')
    if len(classes) > 0:
        printclass = ' class="'
        for hclass in classes:
            newclass = hclass.replace('.', '')
            printclass += newclass+' '
        printclass += '"'
    printclass = printclass.replace(' "', '"')
    return "<"+printtag+printid+printclass+">"+html.escape(content)+"</"+printtag+">"


with open("source.json") as json_file:
    json_data = json.load(json_file)
    sys.stdout.write(html_generation(json_data))
